
def keyFun(node) :
    return node.gain

class Node () :
    def __init__ (self,name,weight,side) :
        self.name = name
        self.weight = weight
        self.side = side
        self.bestSide = side
        self.gain = 0
        self.edges = list()
        self.isLocked = False

    def addEdge(self,e) :
        self.edges.append(e)

    def calculateGain(self,side1,side2) :
        gain = 0 
        for e in self.edges :
            numSide1 = 0
            numSide2 = 0
            for n in e.nodes :
                if n.side == side1 :
                    numSide1 += 1
                elif n.side == side2 :
                    numSide2 += 1
                else :
                    print("Assertion: calculateGain with bug")

            if (numSide1 + numSide2) >= 2 :
                if self.side == side1 :
                    if (numSide1 == 1) and (numSide2 >= 1) :
                        gain += 1
                    elif (numSide1 >= 1) and (numSide2 == 0) :
                        gain -= 1
                elif self.side == side2 :
                    if (numSide2 == 1) and (numSide1 >= 1) :
                        gain += 1
                    elif (numSide2 >= 1) and (numSide1 == 0) :
                        gain -= 1
            else :
                pass
        self.gain = gain

    def setToBest(self) :
        self.bestSide = self.side

    def backToBest(self) :
        self.side = self.bestSide

    def unLock (self) :
        self.isLocked = False

    def lock(self) :
        self.isLocked = True


class Edge () :
    def __init__(self) :
        self.nodes = list()
    def addNode(self,n) :
        self.nodes.append(n)


class NetGraph () :
    def __init__ (self,ratio,side1,side2) :
        self.r = ratio
        self.totalWeight = 0
        self.weights = dict()
        self.bestWeights = dict()
        self.weights[side1] = 0
        self.weights[side2] = 0
        self.bestWeights[side1] = 0 
        self.bestWeights[side2] = 0
        self.nodes = dict()
        self.edges = list()
        self.maxWeight = -1

    def addNode(self,nodeName,nodeWeight,nodeSide) :
        self.nodes[nodeName] = Node(nodeName,nodeWeight,nodeSide)
        self.totalWeight += nodeWeight
        self.weights[nodeSide] += nodeWeight
        self.bestWeights[nodeSide] += nodeWeight
        if nodeWeight > self.maxWeight :
            self.maxWeight = nodeWeight

    def addEdge(self,nodeNameList) :
        e = Edge()
        for nn in nodeNameList :
            node = self.nodes[nn]
            e.addNode(node)

        for node in e.nodes :
            node.addEdge(e)

        self.edges.append(e)

    def isBalanced(self,n,observeSide) :
        upperBound = self.r*self.totalWeight+self.maxWeight
        lowerBound = self.r*self.totalWeight-self.maxWeight
        w = 0
        if n.side == observeSide :
            w = self.weights[observeSide] - n.weight
        else :
            w = self.weights[observeSide] + n.weight

        if (w > upperBound) or (w < lowerBound) :
            return False,w
        else :
            return True,w

    # Because the FM will only care one side balanced or not, we need to choose observeSide
    def runOneIteraion(self,side1,side2,observeSide) :
        # set initial to best as initial best
        self.setToBest()

        # For easy to sorting
        nodesTemp = list()
        for k,node in self.nodes.items() :
            nodesTemp.append(node)
            # unlock all nodes
            node.unLock()

        print ("{} {} {} |{}| {} {} {} {}".format(str('Step').rjust(4),str('Cell').rjust(5),str('MaxGain').rjust(8),str(observeSide).rjust(3),str('Balanced?').rjust(10),str('Locked cells').rjust(20),str('side1').rjust(20),str('side2').rjust(20)))
        
        # Traverse all single swap 
        step = 1 
        maxPartialSum = 0
        partialSum = 0
        while True :
            # calculate gain of each node
            for k,node in self.nodes.items() :
                node.calculateGain(side1,side2)
            # Sorting by gains of nodes
            nodesTemp.sort(key=keyFun,reverse=True)

            # isFinish will be true while we can not move any cell ( all locked or balance issue)
            isFinish = False

            # Find node in decremental order of gain of movements 
            for n in nodesTemp :
                # Only move cells which are not locked
                if n.isLocked == False :
                    # Check balance issue
                    isB,w = self.isBalanced(n,observeSide)
                    # is balance after moving
                    if isB == True :
                        # lock this cell
                        n.lock()
                        # move this cell
                        if n.side == side1 :
                            n.side = side2
                            self.weights[side1] -= n.weight
                            self.weights[side2] += n.weight
                        else :
                            n.side = side1
                            self.weights[side1] += n.weight
                            self.weights[side2] -= n.weight
                        print ("{} {} {} {}      Yes     {}   {}   {}".format(str(step).rjust(4),str(n.name).rjust(5),str(n.gain).rjust(8),str(w).rjust(5),str(self.getLockedNode()).rjust(20),str(self.getCellSide(side1)).rjust(20),str(self.getCellSide(side2)).rjust(20)))
                        step += 1
                        partialSum += n.gain

                        # record best 
                        if partialSum > maxPartialSum :
                            maxPartialSum = partialSum
                            self.setToBest()
                        elif partialSum == maxPartialSum :
                            if abs(self.weights[observeSide] - self.totalWeight/2) < abs(self.bestWeights[observeSide] - self.totalWeight/2) :
                                # More balance
                                self.setToBest()


                        # Next movement
                        break
                    else :
                        print ("{} {} {} {}      No      ------------------------------------------------------------------     ".format(str(step).rjust(4),str(n.name).rjust(5),str(n.gain).rjust(8),str(w).rjust(5)))
                        # last one fail
                        if n == nodesTemp[-1] :
                            isFinish = True
                else :
                    # last one fail
                    if n == nodesTemp[-1] :
                        isFinish = True

            # Can not move any more
            if isFinish == True :
                break

        # restore best result
        self.restoreToBest()

        # print best partition
        print ("\n\n=>Result : ")
        print ("{} {}".format(side1,self.getCellSide(side1)))
        print ("{} {}".format(side2,self.getCellSide(side2)))

        return maxPartialSum

    def getLockedNode(self) :
        lockedCell = ""
        for k,node in self.nodes.items() :
            if node.isLocked == True :
                lockedCell += node.name
                lockedCell += " "
        return lockedCell

    def getCellSide(self,side) :
        cells = ""
        for k,node in self.nodes.items() :
            if node.side == side :
                cells += node.name
                cells += " "
        return cells

    def printBound(self) :
        upperBound = self.r*self.totalWeight+self.maxWeight
        lowerBound = self.r*self.totalWeight-self.maxWeight
        print("upperBound,lowerBound:",upperBound,lowerBound)

    def setToBest(self) :
        for k,v in self.weights.items() :
            self.bestWeights[k] = v
        for k,node in self.nodes.items() :
            node.setToBest()

    def restoreToBest(self) :
        for k,v in self.bestWeights.items() :
            self.weights[k] = v
        for k,node in self.nodes.items() :
            node.backToBest()

    def FM(self,side1,side2,observeSide) :
        ite = 0
        while True :
            ite += 1
            print("\n\n{}-th iteration :".format(ite))
            maxPartial = self.runOneIteraion(side1,side2,observeSide)
            if maxPartial <= 0 :
                break

if __name__ == '__main__' :
    '''
        Please note that the original algorithm (That is FM) is linear time in each iteration.
        But I will implement it with higher complexity than original version for simplifying implementation
    '''

    network = NetGraph(0.3,'A','B')

    # add nodes
    network.addNode('c1',2,'A')
    network.addNode('c2',4,'B')
    network.addNode('c3',6,'A')
    network.addNode('c4',1,'B')
    network.addNode('c5',3,'A')
    network.addNode('c6',5,'B')

    # add edges
    network.addEdge(['c1','c2'])
    network.addEdge(['c1','c4','c5'])
    network.addEdge(['c1','c2','c3','c5'])
    network.addEdge(['c3','c6'])

    network.printBound()
    # network.runOneIteraion('A','B','A')
    network.FM('A','B','A')



