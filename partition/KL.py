
def keyFun (g) :
    return g.g

class Gain () :
    def __init__(self,nodeName1,nodeName2,gain) :
        self.nn1 = nodeName1
        self.nn2 = nodeName2
        self.g = gain

class Node () :
    def __init__(self,name,side) :
        self.n = name # name of this node
        self.s = side # side of this node
        self.isLocked = False 
        self.bestSide = side
        self.I = 0 
        self.E = 0

class Edge () :
    def __init__ (self,nodeName1,nodeName2,weight) :
        self.nn1 = nodeName1
        self.nn2 = nodeName2
        self.w = weight

class NetGraph () :
    def __init__ (self) :
        self.edges = list()
        self.nodes = dict()

    def addNode (self,nodeName,nodeSide) :
        self.nodes[nodeName] = Node(nodeName,nodeSide)

    def addEdge (self,nodeName1,nodeName2,weight) :
        self.edges.append(Edge(nodeName1,nodeName2,weight))

    def resetAllEI (self) :
        for key,node in self.nodes.items() :
            node.I = 0
            node.E = 0

    def calculateAllEI(self) :
        # reset E and I of each node
        self.resetAllEI()

        # calculte E and I of each node
        for e in self.edges :
            if self.nodes[e.nn1].s == self.nodes[e.nn2].s :
                self.nodes[e.nn1].I += e.w
                self.nodes[e.nn2].I += e.w
            else :
                self.nodes[e.nn1].E += e.w
                self.nodes[e.nn2].E += e.w

    def printAllEID(self) :
        print("name : ",end='')
        for key,node in self.nodes.items() :
            name = str(node.n)
            name = name.rjust(6)
            print(name,end='')
        
        print("\nE    : ",end='')
        for key,node in self.nodes.items() :
            E = str(node.E)
            E = E.rjust(6)
            print(E,end='')

        print("\nI    : ",end='')
        for key,node in self.nodes.items() :
            I = str(node.I)
            I = I.rjust(6)
            print(I,end='')

        print("\nD    : ",end='')
        for key,node in self.nodes.items() :
            D = str(node.E - node.I)
            D = D.rjust(6)
            print(D,end='')
        print("")

    def runOneIteration(self,side) :
        # unlock all nodes
        self.unLockAllNode()

        # reset EI
        self.calculateAllEI()

        # iteration time
        ite = 0 
        partialGain = 0
        maxPartialGain = 0
        while True :
            ite += 1
            print("\n    "+str(ite)+"-th swap:")
            gains = list()
            for k1,n1 in self.nodes.items() :
                for k2,n2 in self.nodes.items() :
                    # we only need to calculate swap(a,b) or swap(b,a)
                    if n1.s == side :
                        if (n1.s != n2.s) and (n1.isLocked == False) and (n2.isLocked == False) :
                            g = (n1.E - n1.I) + (n2.E - n2.I) 
                            for e in self.edges :
                                if (e.nn1 == n1.n) and (e.nn2 == n2.n) :
                                    g -= 2*(e.w)
                                elif (e.nn2 == n1.n) and (e.nn1 == n2.n) :
                                    g -= 2*(e.w)
                            gains.append(Gain(n1.n, n2.n,g))

            if len(gains) == 0 :
                print("Nothing to swap")
                break 
            else :
                # print E , I ,and D
                self.printAllEID()
                # print gain table
                print("\nSwap : ",end='')
                for g in gains :
                    p = "(" + g.nn1 +',' + g.nn2 + ")"
                    p = p.rjust(8)
                    print(p,end='')

                print("\nGain : ",end='')
                for g in gains :
                    p = str(g.g)
                    p = p.rjust(8)
                    print(p,end='')
                print()

                # Swap with maximum gain
                gains.sort(key=keyFun,reverse=True)
                print("\n=> Swap (",str(gains[0].nn1),',',str(gains[0].nn2),') with gain = ',str(gains[0].g))

                
                side1 = self.nodes[gains[0].nn1].s 
                side2 = self.nodes[gains[0].nn2].s 

                self.nodes[gains[0].nn1].s = side2
                self.nodes[gains[0].nn1].isLocked = True
                self.nodes[gains[0].nn2].s = side1
                self.nodes[gains[0].nn2].isLocked = True
                self.calculateAllEI()

                # record best  
                partialGain += gains[0].g
                if maxPartialGain < partialGain :
                    maxPartialGain = partialGain
                    self.setToBest()

        # restore to best
        self.restoreToBest()

        return maxPartialGain

    def unLockAllNode(self) :
        for key,node in self.nodes.items() :
            node.isLocked = False

    def setToBest(self) :
        for key,node in self.nodes.items() :
            node.bestSide = node.s

    def restoreToBest(self) :
        for key,node in self.nodes.items() :
            node.s = node.bestSide

    def KL(self,side=-1) :
        # default of side
        if side == -1 :
            side = list(self.nodes.values())[0].s

        # iteration
        ite = 0
        while True :
            ite += 1
            print("\n****************************"+str(ite)+"-th iteration************************************************")
            maxPartialGain = self.runOneIteration(side)
            if maxPartialGain <= 0 :
                break

    def printResult(self) :
        print("Result :")
        # print side
        for key,node in self.nodes.items() :
            print("node "+str(node.n)+" in side : " + str(node.s))

        # print cut
        cutCost = 0
        for e in self.edges :
            if self.nodes[e.nn1].s != self.nodes[e.nn2].s :
                cutCost += e.w 
        print("\n=> cost : "+str(cutCost))






if __name__ == "__main__" :

    network = NetGraph()


    # add nodes
    network.addNode('a','A')
    network.addNode('c','A')
    network.addNode('e','A')
    network.addNode('b','B')
    network.addNode('d','B')
    network.addNode('f','B')

    # add edges
    network.addEdge('a','c',3)
    network.addEdge('a','d',4)
    network.addEdge('b','c',5)
    network.addEdge('b','d',2)
    network.addEdge('b','e',1)
    network.addEdge('c','f',4)
    network.addEdge('d','e',6)
    network.addEdge('d','f',3)
    network.addEdge('e','f',7)

    # print E and I 
    print("Initial setting:")
    network.calculateAllEI()
    network.printAllEID()
    

    # Run KL
    network.KL()
    #network.KL('A')
    print("\n\n\n\n\n\n\n")
    network.printResult()

    
            


