# pip3 install sympy
import numpy as np 
import math 
import sys 

class Point () :
    def __init__ (self,x,y,isFix,name) :
        self.x = x 
        self.y = y 
        self.isFix = isFix 
        self.name = name 
        # For indentify index in matrix
        self.idx = -1
        self.idy = -1 

class Edge () :
    def __init__ (self,point1,point2,weight) :
        self.p1 = point1
        self.p2 = point2
        self.w  = weight


if __name__ == "__main__" :
    # # 2019 PD prob 02
    # # points
    # points = dict()
    # points['p1'] = Point(1,20,True,'p1')
    # points['p2'] = Point(25,3,True,'p2')
    # points['p3'] = Point(3,16,True,'p3')
    # points['p4'] = Point(15,10,True,'p4')
    # points['p5'] = Point(0,0,False,'p5')

    # # edges
    # edges = []
    # edges.append(Edge('p1','p5',1))
    # edges.append(Edge('p2','p5',2))
    # edges.append(Edge('p3','p5',4))
    # edges.append(Edge('p4','p5',8))


    # 2019 PD prob05
    # points
    points = dict()
    points['p1'] = Point(0,0,False,'p1')
    points['p2'] = Point(0,0,False,'p2')
    points['p3'] = Point(0,0,False,'p3')
    points['p4'] = Point(20,5,True,'p4')
    points['p5'] = Point(5,40,True,'p5')
    points['p6'] = Point(25,25,True,'p6')

    # edges
    edges = []
    edges.append(Edge('p1','p2',1))
    edges.append(Edge('p1','p3',1))
    edges.append(Edge('p1','p4',1))
    edges.append(Edge('p2','p4',1))
    edges.append(Edge('p2','p5',1))
    edges.append(Edge('p3','p6',1))


    # # 2018 PD prob05
    # # points
    # points = dict()
    # points['p1'] = Point(0,0,False,'p1')
    # points['p2'] = Point(0,0,False,'p2')
    # points['p3'] = Point(0,0,False,'p3')
    # points['p4'] = Point(25,5,True,'p4')
    # points['p5'] = Point(0,40,True,'p5')
    # points['p6'] = Point(30,30,True,'p6')

    # # edges
    # edges = []
    # edges.append(Edge('p1','p2',1))
    # edges.append(Edge('p1','p3',1))
    # edges.append(Edge('p1','p4',1))
    # edges.append(Edge('p2','p4',1))
    # edges.append(Edge('p2','p5',1))
    # edges.append(Edge('p3','p6',1))



    '''
        Ax = b is [ equation : x of free point 1 ]
                    equation : y of free point 1
                    equation : x of free point 2
                    equation : y of free point 2
                                .
                                .
                                .
                    equation : x of free point N
                    equation : y of free point N

    '''

    # For print equation
    varlist = []
    # calculate matrix size
    matrixSize = 0 
    for n,p in points.items() :
        if p.isFix == False :
            varlist.append(str(p.name+'_X'))
            varlist.append(str(p.name+'_Y'))
            p.idx = matrixSize
            p.idy = matrixSize+1
            matrixSize += 2

    # print size of matrix
    print("matrixSize is :",matrixSize,"x",matrixSize)

    # matrix Ax = b
    A = np.zeros((matrixSize,matrixSize))
    b = np.zeros(matrixSize)
    x = np.zeros(matrixSize)
    # print(A)
    # print(b)

    # Build matrix
    rowIndex = 0
    for n,p in points.items() :
        # from free points to build
        if p.isFix == False :
            # Find edges related to this free point
            for e in edges :
                if (e.p1 == p.name) or (e.p2 == p.name) :
                    '''
                        Because the formula of force is w(p.x-pOther.x) + ... = 0, I need to 
                        change the order to make e.p1 as p. 
                    '''
                    if e.p2 == p.name :
                        e.p2 = e.p1
                        e.p1 = p.name 

                    # It is useless to consider edge with two fixed points
                    if (points[e.p1].isFix == True ) and (points[e.p2].isFix == True) :
                        # Two fixed points
                        continue

                    if points[e.p1].isFix == False :
                        A[rowIndex][ points[e.p1].idx ] += e.w
                        A[rowIndex+1][ points[e.p1].idy ] += e.w
                    else :
                        b[rowIndex]     -=  e.w * points[e.p1].x 
                        b[rowIndex+1]   -=  e.w * points[e.p1].y

                    if points[e.p2].isFix == False :
                        A[rowIndex][ points[e.p2].idx ] -= e.w
                        A[rowIndex+1][ points[e.p2].idy ] -= e.w
                    else :
                        b[rowIndex]     +=  e.w * points[e.p2].x 
                        b[rowIndex+1]   +=  e.w * points[e.p2].y

                else :
                    continue

            rowIndex += 2

    # Assertion
    if rowIndex != matrixSize :
        print("something wrong")
    else :
        # print equation
        print("A and b:")
        print(A,b,end='\n\n')
        print("inv(A):")
        print(np.linalg.inv(A),end='\n\n')
        pass

    
    # print equation
    print("equation:")
    for r in range(A.shape[0]) :
        for c in range(A.shape[1]) :
            print(str(A[r][c])+"*"+str(varlist[c]),end=' ')
            if c != (A.shape[1] - 1) :
                print('+',end=' ')
        print('= ',b[r])


    print("\n\n\nresult:")
    result = np.dot(np.linalg.inv(A),b)
    for n,p in points.items() :
        if p.isFix == False :
            print(p.name," in ",str((result[p.idx],result[p.idy])))




