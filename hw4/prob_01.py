import sys
import numpy as np
import math

# define max limit
MAXINT = 10000000000
MININT = -10000000000



class Point () :
    def __init__ (self,x,y) :
        self.x = x
        self.y = y


def calHPWL(list_in) :
    xmax = MININT
    xmin = MAXINT
    ymax = MININT
    ymin = MAXINT

    for p in list_in :
        if p.x > xmax :
            xmax = p.x
        if p.x < xmin :
            xmin = p.x
        if p.y > ymax :
            ymax = p.y
        if p.y < ymin :
            ymin = p.y 

    return ((xmax-xmin)+(ymax-ymin))

def calSquaredEuclideanDistance(list_in,r) :
    result = 0
    for i in range(len(list_in)) :
        for j in range(i+1,len(list_in)) :
            result += r*((list_in[i].x - list_in[j].x)**2)
            result += r*((list_in[i].y - list_in[j].y)**2)
    return result

def calLogSumExp(list_in,r) :
    s1 = 0 
    s2 = 0 
    s3 = 0
    s4 = 0
    for p in list_in :
        s1 += math.exp(p.x/r)
        s2 += math.exp(-p.x/r)
        s3 += math.exp(p.y/r)
        s4 += math.exp(-p.y/r)

    result = math.log(s1) + math.log(s2) + math.log(s3) + math.log(s4)
    result *= r

    return result 

def calWeightedAverage(list_in,r) :
    s1_u = 0
    s1_d = 0 

    s2_u = 0
    s2_d = 0

    s3_u = 0
    s3_d = 0 

    s4_u = 0
    s4_d = 0

    for p in list_in :
        s1_u += p.x *(math.exp(p.x/r))
        s1_d += math.exp(p.x/r)
        s2_u += p.x *(math.exp(-p.x/r))
        s2_d += math.exp(-p.x/r)

        s3_u += p.y *(math.exp(p.y/r))
        s3_d += math.exp(p.y/r)
        s4_u += p.y *(math.exp(-p.y/r))
        s4_d += math.exp(-p.y/r)

    result = (s1_u/s1_d) - (s2_u/s2_d) + (s3_u/s3_d) - (s4_u/s4_d)

    return result 


if __name__ == "__main__" :
    '''
        Please note that I only consider a line in this program.
        That is, points list only contains points of a line
    '''

    points = []
    # 2019 PD
    points.append(Point(18,3))
    points.append(Point(8,25))
    points.append(Point(4,16))
    points.append(Point(14,5))

    # # 2017 PD
    # points.append(Point(20,2))
    # points.append(Point(8,18))
    # points.append(Point(3,10))
    # points.append(Point(12,4))


    d1 = calHPWL(points)
    print ("HPWL: " + str(d1) )
    d2 = calSquaredEuclideanDistance(points,1)
    print("Squared Euclidean Distance: " + str(d2))
    d3 = calLogSumExp(points,10)
    print("log-sum-exp : " + str(d3))
    d4 = calWeightedAverage(points,10)
    print("weighted Average : " + str(d4))





