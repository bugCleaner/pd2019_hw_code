import sys
import numpy as np
import math

# define max limit
MAXINT = 10000000000
MININT = -10000000000


class Point () :
    def __init__ (self,x,y) :
        self.x = x
        self.y = y


def calHPWL(list_in) :
    xmax = MININT
    xmin = MAXINT
    ymax = MININT
    ymin = MAXINT

    for p in list_in :
        if p.x > xmax :
            xmax = p.x
        if p.x < xmin :
            xmin = p.x
        if p.y > ymax :
            ymax = p.y
        if p.y < ymin :
            ymin = p.y 

    return ((xmax-xmin)+(ymax-ymin))


if __name__ == '__main__' :

    points = []
    # 2019 PD
    points.append(Point(0,0))
    points.append(Point(12,0))
    points.append(Point(12,6))
    points.append(Point(24,0))

    d1 = calHPWL(points)
    print ("HPWL of (1): " + str(d1) )


    points = []
    # 2019 PD
    points.append(Point(0,0))
    points.append(Point(12,0))
    points.append(Point(12,8))
    points.append(Point(24,0))
    points.append(Point(4,4))
    d2 = calHPWL(points)
    print ("HPWL of (2): " + str(d2) )


    points = []
    # 2019 PD
    points.append(Point(0,0))
    points.append(Point(12,0))
    points.append(Point(12,6))
    points.append(Point(24,0))
    points.append(Point(6,6))
    points.append(Point(18,6))
    d3 = calHPWL(points)
    print ("HPWL of (3): " + str(d3) )

