

class Point () :
    def __init__(self,x,y,w) :
        self.x = x
        self.y = y
        self.w = w

def calZeroForce(list_in) :
    w_sum = 0 # weight sum
    wx_sum = 0 # weighted x sum
    wy_sum = 0 # weighted y sum
    for p in list_in :
        w_sum += p.w
        wx_sum += p.w * p.x 
        wy_sum += p.w * p.y

    x = wx_sum/w_sum 
    y = wy_sum/w_sum

    return x,y

if __name__ == "__main__" :
    points = list()

    # 2019 PD
    points.append(Point(1,20,1))
    points.append(Point(25,3,2))
    points.append(Point(3,16,4))
    points.append(Point(15,10,8))

    # # 2017 PD
    # points.append(Point(2,18,1))
    # points.append(Point(18,1,3))
    # points.append(Point(3,16,5))
    # points.append(Point(15,8,7))

    x,y = calZeroForce(points)
    print("Zero force position is : " + str((x,y)))



    
    

